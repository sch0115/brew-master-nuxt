import Vue from 'vue'
import VueResource from 'vue-resource'

const MODULE_NAME = 'HttpService'

Vue.use(VueResource)

Vue.http.options.root = 'api/'

// Interceptors for token authentication
// Vue.http.interceptors.push((request, next) => {
//   const token =
//     localStorage.getItem('token') || sessionStorage.getItem('token');
//   if (request.body && request.body.email && request.body.email.toLowerCase) {
//     request.body.email = request.body.email.toLowerCase();
//   }
//
//   if (token !== null && !request.isExternal) {
//     request.headers.set('Authorization', token);
//   }
//
//   next(response => {
//     /*
//      * Add a global logout for any 401 Unauthorised returns.
//      */
//     if (Boolean(response.status === 401) && Boolean(token)) {
//       AuthService.logOut();
//     }
//   });
// });

export default {
  beerName: MODULE_NAME,

  /**
   * HTTP Get
   * @param url {string}
   * @param options {object}
   * @return {Promise.<T>|*}
   */
  get(url, options = {}) {
    return Vue.http.get(url, options)
  },
  getJson(url) {
    return Vue.getJson(url)
  },
  getExternal(url, options = { isExternal: true }) {
    return Vue.http.get(url, options)
  },

  /**
   * HTTP Post
   * @param url {string}
   * @param body {object}
   * @param options {object}
   * @return {Promise.<T>|*}
   */
  post(url, body, options = {}) {
    return Vue.http.post(url, body, options)
  }
}
