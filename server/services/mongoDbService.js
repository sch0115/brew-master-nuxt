/* eslint-disable no-console */
import mongo from '../util/database'
const { ObjectId } = require('mongodb')
const COLLECTION = 'brew-master'

const insertOne = async function(document) {
  const db = await mongo.getDb()

  if (db) {
    db.collection(COLLECTION)
      .insertOne(document)
      .then(result => {
        console.log('inserted ', result, ' into DB')
      })
      .catch(err => console.log(err))
  } else {
    console.error('Database not connected! Environment data were not saved!')
  }
}
const updateOne = async function(document) {
  const db = await mongo.getDb()

  if (db) {
    db.collection(COLLECTION)
      .update({ _id: ObjectId(document._id) }, { $set: document })
      .then(result => {
        console.log('inserted ', result, ' into DB')
      })
      .catch(err => console.log(err))
  } else {
    console.error('Database not connected! Environment data were not saved!')
  }
}

const getDataByQueryInTimeRange = async (query, from, to) => {
  let data
  const db = await mongo.getDb()

  if (db) {
    data = await db
      .collection(COLLECTION)
      .find({
        ...query,
        timestamp: {
          $gte: from
        }
      })
      .sort({ _id: -1 })
      .toArray()
  } else {
    console.error(
      'Database not connected! Current data of ' + query + 'was not acquired!'
    )
  }
  return data
}

const getCurrentBrewConfig = async function() {
  let currentBrewConfig
  const db = await mongo.getDb()

  if (db) {
    currentBrewConfig = await db
      .collection(COLLECTION)
      .find({ entityType: 'BrewConfig' })
      .sort({ _id: -1 })
      .limit(1)
      .toArray()
  } else {
    console.error('Database not connected! Environment data were not saved!')
  }
  return currentBrewConfig[0]
}
const getLatestByQuery = async function(query) {
  let currentBrewConfig
  const db = await mongo.getDb()

  if (db) {
    currentBrewConfig = await db
      .collection(COLLECTION)
      .find(query)
      .sort({ _id: -1 })
      .limit(1)
      .toArray()
  } else {
    console.error('Database not connected! getLatest', query)
  }
  return currentBrewConfig[0]
}

export default {
  getDataByQueryInTimeRange,
  getCurrentBrewConfig,
  insertOne,
  getLatestByQuery,
  updateOne
}
