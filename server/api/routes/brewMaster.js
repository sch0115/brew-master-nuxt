/* eslint-disable no-console */
const { Router } = require('express')
const brewService = require('../../services/brewService').default

const router = Router()

/* GET current brew data for current brew. */
router.get('/brew', async function(req, res, next) {
  const currentBrew = await brewService.getCurrentBrew()
  res.json(currentBrew)
})
/* GET current BrewBox state. */
router.get('/brewState', async function(req, res, next) {
  const latestBrewBoxState = await brewService.getCurrentBrewState()
  res.json(latestBrewBoxState)
})
/* GET current brew data for current brew. */
router.get('/brewData', async function(req, res, next) {
  const currentBrew = await brewService.getCurrentBrew()
  const brewData = await brewService.getBrewData(currentBrew)
  res.json(brewData)
})
/* GET current brew config. */
router.get('/brewConfig', async function(req, res, next) {
  const latestBrewBoxState = await brewService.getCurrentBrewConfig()
  res.json(latestBrewBoxState)
})
//* POST new brew config. */de
router.post('/brewConfig', async function(req, res, next) {
  const brewConfig = await brewService.createNewBrewConfig(req.body)
  res.json(brewConfig)
})
/* POST create new brew */
router.post('/startNewBrew', async function(req, res, next) {
  const currentBrew = await brewService.startNewBrew(req.body)
  res.json(currentBrew)
})
/* POST create new brew */
router.post('/brew', async function(req, res, next) {
  try {
    await brewService.upsertBrew(req.body)
    res.statusCode(204)
  } catch (e) {
    res.statusCode(400, e)
  }
})
/* POST create new brew */
router.post('/endCurrentBrew', async function(req, res, next) {
  try {
    await brewService.endCurrentBrew()
  } catch (e) {
    console.log(e)
  }
})

module.exports = router
