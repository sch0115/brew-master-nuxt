/* eslint-disable no-console */
import Vuex from 'vuex'
// import BrewService from '../services/brew.service'

const createStore = () => {
  return new Vuex.Store({
    state: {
      brew: {},
      brewState: {},
      brewData: [],
      brewConfig: {}
    },
    mutations: {
      setBrew(state, brew) {
        state.brew = brew
      },
      setBrewState(state, brewState) {
        console.log('settingBrewState', brewState)
        state.brewState = brewState
      },
      setBrewData(state, brewData) {
        state.brewData = brewData
      },
      addBrewData(state, brewState) {
        state.brewData.push(brewState)
      },
      setBrewConfig(state, brewConfig) {
        state.brewConfig = brewConfig
      }
    },
    // actions: {
    //   loadAll(vuexContext) {
    //     vuexContext.dispatch('loadBrew')
    //     vuexContext.dispatch('loadBrewState')
    //     vuexContext.dispatch('loadBrewData')
    //     vuexContext.dispatch('loadBrewConfig')
    //   },
    //   updateAll(vuexContext) {
    //     vuexContext.dispatch('loadBrew')
    //     vuexContext.dispatch('loadBrewState', true)
    //     vuexContext.dispatch('loadBrewConfig')
    //   },
    //   loadBrew(vuexContext) {
    //     BrewService.getBrew()
    //       .then(brew => {
    //         vuexContext.commit('setBrew', brew)
    //       })
    //       .catch(e => console.error(e))
    //   },
    //   loadBrewState(vuexContext, updateData) {
    //     BrewService.getBrewState()
    //       .then(brewState => {
    //         vuexContext.commit('setBrewState', brewState)
    //         if (updateData) {
    //           vuexContext.commit('addBrewData', brewState)
    //         }
    //       })
    //       .catch(e => console.error(e))
    //   },
    //   loadBrewData(vuexContext) {
    //     BrewService.getBrewData()
    //       .then(data => {
    //         vuexContext.commit('setBrewData', data)
    //       })
    //       .catch(e => console.error(e))
    //   },
    //   loadBrewConfig(vuexContext) {
    //     BrewService.getBrewConfig()
    //       .then(config => {
    //         vuexContext.commit('setBrewConfig', config)
    //       })
    //       .catch(e => console.error(e))
    //   }
    // },
    getters: {
      brew(state) {
        return state.brew
      },
      brewState(state) {
        return state.brewState
      },
      brewData(state) {
        return state.brewData
      },
      brewConfig(state) {
        return state.brewConfig
      }
    }
  })
}

export default createStore
