/* eslint-disable no-console */
import MongoDbService from './mongoDbService'
const ObjectID = require('mongodb').ObjectID

const getCurrentBrewState = async () => {
  const currentState = await MongoDbService.getLatestByQuery({
    component: 'BrewBox',
    entityType: 'stateRecord'
  })
  return currentState
}
const getCurrentBrewConfig = async () => {
  const currentBrewConfig = await MongoDbService.getLatestByQuery({
    entityType: 'brewConfig'
  })
  return currentBrewConfig
}
const createNewBrewConfig = async config => {
  try {
    const configRecord = {
      entityType: 'brewConfig',
      ...config
    }
    await MongoDbService.insertOne(configRecord)
  } catch (e) {
    console.log(e)
  }
}
const getCurrentBrew = async () => {
  const currentBrew = await MongoDbService.getLatestByQuery({
    entityType: 'brew'
  })
  return currentBrew
}
const endCurrentBrew = async () => {
  try {
    const currentBrew = await getCurrentBrew()
    if (currentBrew) {
      currentBrew.state = 'ended'
      currentBrew.end = new Date()
    }
    await upsertBrew(currentBrew)
  } catch (e) {
    console.log(e)
  }
}
const startNewBrew = async brew => {
  try {
    await endCurrentBrew()
    // todo create instance and upsert
    const currentBrew = await upsertBrew({
      entityType: 'brew',
      state: 'active',
      ...brew
    })
    return currentBrew
  } catch (e) {
    console.log(e)
  }
}
const upsertBrew = async brew => {
  try {
    if (brew._id) {
      await MongoDbService.updateOne(brew)
    } else {
      const brewRecord = {
        entityType: 'brew',
        ...brew
      }
      await MongoDbService.insertOne(brewRecord)
    }
  } catch (e) {
    console.log(e)
  }
}
const getBrewData = async brew => {
  const brewData = await MongoDbService.getDataByQueryInTimeRange(
    {
      component: 'BrewBox',
      entityType: 'stateRecord'
    },
    ObjectID(brew._id).getTimestamp()
  )
  return brewData
}

export default {
  getCurrentBrewState,
  getCurrentBrewConfig,
  getCurrentBrew,
  getBrewData,
  startNewBrew,
  createNewBrewConfig,
  endCurrentBrew
}
