/* eslint-disable no-console */

const dateFromObjectId = objectId => {
  return new Date(parseInt(objectId.substring(0, 8), 16) * 1000)
}

module.exports.dateFromObjectId = dateFromObjectId
