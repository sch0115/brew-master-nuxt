/* eslint-disable no-console */
const mongodb = require('mongodb')
const MongoClient = mongodb.MongoClient

let _db

const mongoConnect = async () => {
  let client

  try {
    client = await MongoClient.connect(
      'mongodb+srv://sch0115:Passw0rd01@cluster0-sih4m.mongodb.net/test?retryWrites=true',
      { useNewUrlParser: true }
    )
    console.log('Connected!')
    _db = client.db()
  } catch (e) {
    return console.log('Error while connecting to mongo DB ->', e)
  }
}

exports.mongoConnect = mongoConnect
exports.getDb = async function() {
  if (_db) {
    return _db
  }
  await mongoConnect()
  return _db
}
