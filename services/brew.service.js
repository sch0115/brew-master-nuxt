/* eslint-disable no-console */
import axios from 'axios'

const getBrew = async () => {
  try {
    const response = await axios.get('api/brew')
    return response.data
  } catch (e) {
    console.log(e)
  }
}
const getBrewState = async () => {
  try {
    const response = await axios.get('api/brewState')
    return response.data
  } catch (e) {
    console.log(e)
  }
}
const getBrewData = async () => {
  try {
    const response = await axios.get('api/brewData')
    return response.data
  } catch (e) {
    console.log(e)
  }
}
const getBrewConfig = async () => {
  try {
    const response = await axios.get('api/brewConfig')
    return response.data
  } catch (e) {
    console.log(e)
  }
}
const startNewBrew = async brew => {
  try {
    const response = await axios.get('api/brewConfig')
    return response.data
  } catch (e) {
    console.log(e)
  }
}
const updateBrew = async brew => {
  try {
    const response = await axios.post('api/brew', brew)
    return response.data
  } catch (e) {
    console.log(e)
  }
}
const updateConfig = async config => {
  try {
    const response = await axios.get('api/brewConfig')
    return response.data
  } catch (e) {
    console.log(e)
  }
}

export default {
  getBrew,
  getBrewState,
  getBrewConfig,
  getBrewData,
  startNewBrew,
  updateBrew,
  updateConfig
}
