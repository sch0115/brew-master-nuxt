const express = require('express')

// Create express instnace
const app = express()

// Require API routes
const brewMaster = require('./routes/brewMaster')

app.use(express.json())

// Import API Routes
app.use(brewMaster)

// Export the server middleware
module.exports = {
  path: '/api',
  handler: app
}
