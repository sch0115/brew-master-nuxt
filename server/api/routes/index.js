const { Router } = require('express')
const router = Router()

// Require API routes
const users = require('./routes/users')
const systemInfo = require('./routes/systemInfo')
const brewMaster = require('./routes/brewMaster')

// Require Db connection service

// Import API Routes
router.use(users)
router.use(systemInfo)
router.use(brewMaster)

module.exports = router
